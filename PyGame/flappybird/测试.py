import pygame,time,random
from pygame.locals import *
import sys
#print(pygame.ver)
def game_ini():
    a=0
    welcome_loading = pygame.image.load("依赖包/图片/其他/加载.png")
    screen.blit(welcome_loading,(0,0))
    pygame.display.update()
    time.sleep(0.5)
    welcome_kaishi = pygame.image.load("依赖包/图片/其他/开始.png")
    welcome_ground = pygame.image.load("依赖包/图片/背景/地.png")
    bird_u = pygame.image.load("依赖包/图片/鸟/上0.png")
    bird_m = pygame.image.load("依赖包/图片/鸟/中0.png")
    bird_d = pygame.image.load("依赖包/图片/鸟/下0.png")
    flag = True
    while flag:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x,y = event.pos
                if x>10 and x<170 and y>350 and y<450:
                    flag=False      
        a -= 2
        if a<0:
            a = 384
        screen.blit(welcome_kaishi,(0,0))
        screen.blit(welcome_ground,(a,448))
        screen.blit(welcome_ground,(a - 384,448))
        b = a % 128
        if b>=0 and b<32:
            screen.blit(bird_m,(175,220))
        elif b>=32 and b<64:
            screen.blit(bird_u,(175,216))
        elif b>=64 and b<96:
            screen.blit(bird_m,(175,220))
        elif b>=96 and b<128:
            screen.blit(bird_d,(175,224))        
        fpsclock.tick(77)
        pygame.display.update()

def guide():
    shijian = 0
    daynight = random.choice((1,2))
    if daynight==1:
        course_up = pygame.image.load("依赖包/图片/引导/白天/上.png")
        course_mid = pygame.image.load("依赖包/图片/引导/白天/中.png")
        course_down = pygame.image.load("依赖包/图片/引导/白天/下.png")
    else:
        course_up = pygame.image.load("依赖包/图片/引导/晚上/上.png")
        course_mid = pygame.image.load("依赖包/图片/引导/晚上/中.png")
        course_down = pygame.image.load("依赖包/图片/引导/晚上/下.png")
    img_ground = pygame.image.load("依赖包/图片/背景/地.png")
    flag = True
    while(flag):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == MOUSEBUTTONDOWN:
                flag = False

        shijian+=1
        a=shijian%64
        if a<16:
            screen.blit(course_up,(0,0))
        elif a<32:
            screen.blit(course_mid,(0,0))
        elif a<48:
            screen.blit(course_down,(0,0))
        else:
            screen.blit(course_mid,(0,0))
        
        a-=2
        if a<0:
            a = 384
        screen.blit(img_ground,(a,448))
        screen.blit(img_ground,(a - 384,448))    
        fpsclock.tick(77)
        pygame.display.update()

def game():
    alive=True
    shijian = 0
    bird_h = 250
    point = 0
    zhuzi=[[700,0],[940,0],[1180,0]]
    v=0
    a=0
    b=0
    c=0
    bird_up_0 = pygame.image.load("依赖包/图片/鸟/上0.png")
    bird_middle_0 = pygame.image.load("依赖包/图片/鸟/中0.png")
    bird_down_0 = pygame.image.load("依赖包/图片/鸟/下0.png")
    bird_up_20 = pygame.image.load("依赖包/图片/鸟/上20.png")
    bird_middle_20 = pygame.image.load("依赖包/图片/鸟/中20.png")
    bird_down_20 = pygame.image.load("依赖包/图片/鸟/下20.png")
    bird_up__20 = pygame.image.load("依赖包/图片/鸟/上-20.png")
    bird_middle__20 = pygame.image.load("依赖包/图片/鸟/中-20.png")
    bird_down__20 = pygame.image.load("依赖包/图片/鸟/下-20.png")
    bird_up__90 = pygame.image.load("依赖包/图片/鸟/上-90.png")
    bird_middle__90 = pygame.image.load("依赖包/图片/鸟/中-90.png")
    bird_down__90 = pygame.image.load("依赖包/图片/鸟/下-90.png")
    zhuzi_z = pygame.image.load("依赖包/图片/柱子/正.png")
    zhuzi_f = pygame.image.load("依赖包/图片/柱子/反.png")
    gameover = pygame.image.load("依赖包/图片/其他/gameover.png")
    jifenpai = pygame.image.load("依赖包/图片/其他/记分牌.png")
    again = pygame.image.load("依赖包/图片/其他/重来.png")
    paihangbang = pygame.image.load("依赖包/图片/其他/排行榜.png")
    tong = pygame.image.load("依赖包/图片/奖牌/铜牌.png")
    yin = pygame.image.load("依赖包/图片/奖牌/银牌.png")
    gold = pygame.image.load("依赖包/图片/奖牌/金牌.png")
    baijin = pygame.image.load("依赖包/图片/奖牌/白金牌.png")

    daynight = random.choice((1,2))
    if daynight==1:
        bk = pygame.image.load("依赖包/图片/背景/白天.png")
    else:
        bk = pygame.image.load("依赖包/图片/背景/黑夜.png")
    ground = pygame.image.load("依赖包/图片/背景/地.png")

    while(True):
        shijian+=1           
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == MOUSEBUTTONDOWN :
                if alive ==True and shijian-b>1:
                    v=-180
                    b=shijian
                if alive ==False:
                    x,y = event.pos
                    if x>10 and x<170 and y>350 and y<450:
                        alive = True  
                        shijian = 0
                        bird_h = 250
                        point = 0
                        zhuzi=[[700,0],[940,0],[1180,0]]
                        v=0
                        a=0
                        b=0
                        c=0                      
        ##
        if alive == True:
            v+=9.8
            bird_h+=((v*1.9)//77)
            for c in range(3):
                zhuzi[c][0] -=2
                if zhuzi[c][0] <=-70:
                    zhuzi[c][0]=650
                if zhuzi[c][0]==512:
                    zhuzi[c][1]=random.randint(1,200)+200
            ##算分
            for c in range(3):
                if zhuzi[c][0] == 100:
                    point +=1
            ##显示背景
            screen.blit(bk,(0,0))
            #柱子
            for c in range(3):
                screen.blit(zhuzi_z,(zhuzi[c][0],zhuzi[c][1]))
                screen.blit(zhuzi_f,(zhuzi[c][0],zhuzi[c][1]-400))
            ##地面
            a=shijian%192
            a*=-2
            screen.blit(ground,(a,448))
            screen.blit(ground,(a + 384,448)) 
            ##鸟
            a=shijian%16
            if a<4:
                screen.blit(bird_middle_0,(100,bird_h))
            elif a<8:
                screen.blit(bird_up_0,(100,bird_h))
            elif a<12:
                screen.blit(bird_middle_0,(100,bird_h))
            else:
                screen.blit(bird_down_0,(100,bird_h))
            
            ##碰撞检测
            for c in range(3):
                if zhuzi[c][0]>=35 and zhuzi[c][0]<=135 and (zhuzi[c][1]<(bird_h+27) or (zhuzi[c][1]-155)>bird_h):
                    alive=False
            if bird_h >415:
                alive=False
            ##分数
            font = pygame.font.SysFont("宋体",40)
            score = font.render(str(point),True,(255,255,255))
            screen.blit(score,(180,40))
                    
            fpsclock.tick(77)
            pygame.display.update()
        else:
            while(alive==False):
                screen.blit(bk,(0,0))
                for c in range(3):
                    screen.blit(zhuzi_z,(zhuzi[c][0],zhuzi[c][1]))
                    screen.blit(zhuzi_f,(zhuzi[c][0],zhuzi[c][1]-400))
                a=shijian%192
                a *= -2
                screen.blit(ground,(a,448))
                screen.blit(ground,(a + 384,448))

                a=shijian%16
                if a<4:
                        screen.blit(bird_middle__90,(100,bird_h))
                elif a<8:
                    screen.blit(bird_up__90,(100,bird_h))
                elif a<12:
                    screen.blit(bird_middle__90,(100,bird_h))
                else:
                    screen.blit(bird_down__90,(100,bird_h))
                v+=9.8
                bird_h+=((v*2)/77)
                if bird_h>430:
                    break
            time.sleep(0.3)              
            screen.blit(gameover,(55,60))
            time.sleep(0.3) 
            screen.blit(jifenpai,(35,150))
            time.sleep(0.3) 
            screen.blit(again,(35,350))
            screen.blit(paihangbang,(200,350))
            screen.blit(score,(270,195))
            if point<10:
                pass
            elif point <20:
                screen.blit(tong,(67,200))
            elif point<30:
                screen.blit(yin,(67,200))
            elif point<40:
                screen.blit(gold,(67,200))
            else:
                screen.blit(baijin,(67,200))
            jilufile = open("依赖包/记录.txt")
            jilu = eval(jilufile.readline())
            jilufile.close
            if point>jilu:
                jilu = str(point)
                jilufile=open("依赖包/记录.txt","w")
                jilufile.write(jilu)
                jilufile.close
            font = pygame.font.SysFont("宋体",40)
            jilu = font.render(str(jilu),True,(255,255,255))
            screen.blit(jilu,(270,260))
            fpsclock.tick(66)
            pygame.display.update()
    
if __name__ == "__main__":
    fpsclock = pygame.time.Clock()
    pygame.init()
    window_size = (384,512)
    screen = pygame.display.set_mode(window_size)
    pygame.display.set_caption("FlappyBird 1.0")
    game_ini()
    guide()
    game()
#4人发52张牌，显示每位手里的牌
#梅花0-12，方块13-25，红桃26-38，黑桃39-51
import random

n=52                                #52张牌
pocker = [i for i in range(n)]      #新牌

def gen_poker(n):                   #洗牌函数,洗100次
    x=100
    while(x > 0):
        x = x-1
        p1 = random.randint(0,n-1)
        p2 = random.randint(0,n-1)
        t = pocker[p1]              #交换
        pocker[p1] = pocker[p2]
        pocker[p2] = t
    return pocker

def getColor(x):                    #根据值，确定是什么花色
    color = ["♣︎","♦︎","♥︎","♠︎"]
    c = int(x/13)
    if c<0 or c>3:
        return "没有这种花色！"
    return color[c]

def getValue(x):                    #根据值，确定是什么大小的牌
    value = x % 13
    if value == 0:
        return 'A'
    elif value>=1 and value <=9:
        return str(value+1)
    elif value == 10:
        return "J"
    elif value == 11:
        return "Q"
    elif value == 12:
        return "K"

def getPuk(x):
    return getColor(x) + getValue(x)
#主程序
(a,b,c,d) = ([],[],[],[])           #初始化4个人
pocker = gen_poker(n)
print(pocker)
for x in range(13):
    m = x*4
    a.append(getPuk(pocker[m]))
    b.append(getPuk(pocker[m+1]))
    c.append(getPuk(pocker[m+2]))
    d.append(getPuk(pocker[m+3]))
a.sort()
b.sort()
c.sort()
d.sort()

print("牌手1：",a)
print("牌手2：",b)
print("牌手3：",c)
print("牌手4：",d)
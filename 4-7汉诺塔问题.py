def mov(source,target):
    print(source,"=>",target)

def hanoi(n,source,temp,target):
    if(n==1):
        mov(source,target)
    else:
        hanoi(n-1,source,target,temp)
        mov(source,target)
        hanoi(n-1,temp,source,target)

n=int(input("请输入盘子数目："))
print("移动",n,"个盘子的步骤是：")
hanoi(n,'A','B','C')



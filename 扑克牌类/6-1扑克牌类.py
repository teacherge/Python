#cards module
class Card():
    RANKS = ["A","2","3","4","5","6","7","8","9","10","J","Q","K"]
    SUITS = ["♣︎","♦︎","♥︎","♠︎"]
    def __init__(self,rank,suit,face_up = True):
        self.rank = rank
        self.suit = suit
        self.is_faceup = face_up
    
    def __str__(self):
        if self.is_faceup:
            rep = self.suit+self.rank
        else:
            rep = "XX"
        return rep

    def pic_order(self):
        if self.rank == "A":
            facenum = 1
        elif self.rank == "J":
            facenum = 11
        elif self.rank == "Q":
            facenum = 12
        elif self.rank == "K":
            facenum = 13
        else:
            facenum = int(self.rank)
        
        if self.suit == "♣︎":
            suit = 1
        elif self.suit == "♦︎":
            suit = 2
        elif self.suit == "♥︎":
            suit = 3
        else:
            suit = 4
        return (suit-1)*13 + facenum
    def flip(self):
        self.is_faceup = not self.is_faceup

class Hand():
    def __init__(self):
        self.cards = []
    def __str__(self):
        if self.cards:
            rep = ""
            for card in self.cards:
                rep += str(card) + "\t"
        else:
            rep = "无牌"
        return rep
    def clear(self):
        self.cards = []
    def add(self,card):
        self.cards.append(card)
    def give(self,card,other_hand):
        self.cards.remove(card)
        other_hand.add(card)

class Poke(Hand):
    def populate(self):
        for suit in Card.SUITS:
            for rank in Card.RANKS:
                self.add(Card(rank,suit))
    def shuffle(self):
        import random
        random.shuffle(self.cards)
    
    def deal(self,hands,per_hand =13):
        for round in range(per_hand):
            for hand in hands:
                if self.cards:
                    top_card = self.cards[0]
                    self.cards.remove(top_card)
                    hand.add(top_card)
                else:
                    print("发牌完毕！")

if __name__=="__main__":
    print("游戏开始！\n")
    players = [Hand(),Hand(),Hand(),Hand()]
    poke1 = Poke()
    print("新牌！\n")
    poke1.populate()
    print("洗牌！\n")
    poke1.shuffle()
    print("发牌！\n")
    poke1.deal(players,13)

    n=1
    for hand in players:
        print("牌手",n,end=":")
        print(hand)
        n+=1
    input("\n按任意键结束...")

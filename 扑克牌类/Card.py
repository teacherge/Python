#cards module
class Card():
    RANKS = ["A","2","3","4","5","6","7","8","9","10","J","Q","K"]
    SUITS = ["♣︎","♦︎","♥︎","♠︎"]
    def __init__(self,rank,suit,face_up = True):
        self.rank = rank
        self.suit = suit
        self.is_faceup = face_up
    
    def __str__(self):
        if self.is_faceup:
            rep = self.suit+self.rank
        else:
            rep = "XX"
        return rep

    def pic_order(self):
        if self.rank == "A":
            facenum = 1
        elif self.rank == "J":
            facenum = 11
        elif self.rank == "Q":
            facenum = 12
        elif self.rank == "K":
            facenum = 13
        else:
            facenum = int(self.rank)
        
        if self.suit == "♣︎":
            suit = 1
        elif self.suit == "♦︎":
            suit = 2
        elif self.suit == "♥︎":
            suit = 3
        else:
            suit = 4
        return (suit-1)*13 + facenum
    def flip(self):
        self.is_faceup = not self.is_faceup
import Hand,Poke

if __name__=="__main__":
    print("游戏开始！\n")
    players = [Hand.Hand(),Hand.Hand(),Hand.Hand(),Hand.Hand()]
    poke1 = Poke.Poke()
    print("新牌！\n")
    poke1.populate()
    print("洗牌！\n")
    poke1.shuffle()
    print("发牌！\n")
    poke1.deal(players,13)

    n=1
    for hand in players:
        print("牌手",n,end=":")
        print(hand)
        n+=1
    input("\n按任意键结束...")
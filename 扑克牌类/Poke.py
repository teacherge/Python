import Card
import Hand
class Poke(Hand.Hand):
    def populate(self):
        for suit in Card.Card.SUITS:
            for rank in Card.Card.RANKS:
                self.add(Card.Card(rank,suit))
    def shuffle(self):
        import random
        random.shuffle(self.cards)
    
    def deal(self,hands,per_hand =13):
        for round in range(per_hand):
            for hand in hands:
                if self.cards:
                    top_card = self.cards[0]
                    self.cards.remove(top_card)
                    hand.add(top_card)
                else:
                    print("发牌完毕！")
#九宫格内，先连成3子，横竖斜胜利
#全局常量
X = "😀"
O = "😡"
EMPTY = ""
#询问是否继续
def ask_yesno(question):
    res = None
    while res not in ("y","n"):
        res = input(question).lower()
    return res
#输入位置数字
def ask_number(question,low,high):
    res = None
    while res not in range(low,high):
        res = int(input(question))
    return res
#询问谁先走
def pieces():
    go_first = ask_yesno("你是否先走(y/n):")
    if go_first =="y":
        print("玩家请先走。")
        human = X
        computer = O
    else:
        print("计算机先走。")
        computer = X
        human = O
    return computer,human
#生成新的棋盘
def new_board():
    board = []
    for square in range(9):
        board.append(EMPTY)
    return board
#显示棋盘
def display_board(board):
    board2 = board[:]
    for i in range(len(board)):
        if board[i]==EMPTY:
            board2[i] = i
    print("\t",board2[0],"|",board2[1],"|",board2[2])
    print("\t","----------")
    print("\t",board2[3],"|",board2[4],"|",board2[5])
    print("\t","----------")
    print("\t",board2[6],"|",board2[7],"|",board2[8])
#产生可以合法走棋位置序列（还未下过子的位置）
def legal_moves(board):
    moves = []
    for square in range(9):
        if board[square] == EMPTY:
            moves.append(square)
    return moves    
#判断输赢
def winner(board):
    #把所有能赢得线路事先列出来，三行三列，对角线
    WAYS_TO_WIN = ((0,1,2),(3,4,5),(6,7,8),(0,3,6),(1,4,7),(2,5,8),(0,4,8),(2,4,6))
    for row in WAYS_TO_WIN:
        if board[row[0]] == board[row[1]] == board[row[2]] != EMPTY:
            winner = board[row[0]]
            return winner
    if EMPTY not in board:
        return "TIE"
    return False
#人走棋
def human_move(board,human):
    legal = legal_moves(board)
    move = None
    while move not in legal:
        move = ask_number("你要走哪个位置?(0-8):",0,9)
        if move not in legal:
            print("\n此位置已经落子了")
    return move
#计算机走棋
def computer_move(board,computer,human):
    board2 = board[:]                   #创建副本，不影响原来的棋盘
    BEST_MOVES = (4,0,2,6,8,1,3,5,7)
    #如果计算机能赢，就走那个位置
    for move in legal_moves(board2):
        board2[move] = computer
        if winner(board2) == computer:
            print("计算机下棋位置：",move)
            return move
        board2[move] = EMPTY
    #如果人能赢，就走那个位置
    for move in legal_moves(board2):
        board2[move] = human
        if winner(board2) == human:
            print("计算机下棋位置：",move)
            return move
        board2[move] = EMPTY
    #如果以上都不满足，从最佳下棋位置表中挑选第一个合法位置
    for move in BEST_MOVES:
        if move in legal_moves(board2):
            print("计算机下棋位置：",move)
            return move
#轮换走棋的角色
def next_turn(turn):
    if turn == X:
        return O
    else:
        return X
#主函数
def main():
    computer,human = pieces()
    turn = X
    board = new_board()
    display_board(board)
    while not winner(board):
        if turn == human:
            move = human_move(board,human)
            board[move] = human
        else:
            move = computer_move(board,computer,human)
            board[move] = computer
        display_board(board)
        turn = next_turn(turn)
    the_winner = winner(board)
    if the_winner == computer:
        print("计算机胜利！\n")
    elif the_winner == human:
        print("玩家胜利！\n")
    elif the_winner == "TIE":        
        print("平局和棋，游戏结束！\n")

#游戏开始
main()
input("按任意键退出游戏...")   











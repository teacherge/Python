#对输入的成绩进行ABCDE等级划分，成绩的范围在[0,100]区间，
# 如果输入的成绩不在这个范围，提示用户输入错误。
degree = 'DCBAE'
mark = int(input('请输入成绩：'))
if mark<0 or mark>100:
    print("Error")
else:
    grade = (mark-60)//10
    if grade>=0:
        print(degree[grade])
    else:
        print(degree[-1])
#对于百分制的成绩，我们可以将其转化为等级制。
# 标准是成绩在90分及以上为等级A，80至89为等级B，70至79为等级C，60至69为等级D，60分以下为等级E。
mark = int(input("请输入成绩："))
if mark>=90:
    grade = 'A'
elif mark>=80:
    grade = 'B'
elif mark>=70:
    grade = 'C'
elif mark>=60:
    grade = 'D'
else:
    grade = 'E'
print(mark,'分的等级为：',grade)